require 'test_helper'

class StatusTest < ActiveSupport::TestCase
  test "that a status required a content" do
    status = Status.new
    assert !status.save
    assert !status.errors[:content].empty?
  end  
  
  test "that a status' content have at least 2 letter" do
    status = Status.new
    status.content = "H"
    assert !status.save
    assert !status.errors[:content].empty?
  end
  
  test "that a status should have a user" do
    status = Status.new
    
    assert !status.save
    assert !status.errors[:user_id].empty?
  end
end
